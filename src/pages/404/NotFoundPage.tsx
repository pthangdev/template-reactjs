import type { FC } from 'react';

interface NotFoundPageProps {}

const NotFoundPage: FC<NotFoundPageProps> = (props) => {
  return <div>NotFoundPage</div>;
};

export default NotFoundPage;
