export { default as NotFoundPage } from './404/NotFoundPage';
export { default as LoginPage } from './auth/login/LoginPage';
export { default as RegisterPage } from './auth/register/RegisterPage';
export { default as HomePage } from './home/HomePage';
