import { FC } from 'react';
import GlobalTheme from './configs/themes';
import { PageRoutes } from './routes';

interface AppProps {}

const App: FC<AppProps> = (props) => {
  return (
    <GlobalTheme>
      <PageRoutes />
    </GlobalTheme>
  );
};

export default App;
