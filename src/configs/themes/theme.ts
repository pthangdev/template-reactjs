import { PaletteMode, ThemeOptions } from '@mui/material';

const getTheme = (themeMode: PaletteMode = 'dark') => {
  const theme: ThemeOptions = {
    palette: {
      mode: themeMode,
    },

    components: {
      MuiButton: {
        defaultProps: {
          variant: 'contained',
        },
      },
    },
  };
  return theme;
};

export default getTheme;
