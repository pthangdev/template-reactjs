import { createTheme, ThemeProvider as MuiThemeProvider } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import { GlobalStyles } from '@mui/styled-engine-sc';
import { FC, ReactNode, useContext } from 'react';
import { ThemeContext } from '~/app/contexts';
import globalStyles from './globalStyles';
import getTheme from './theme';

interface ThemeProviderProps {
  children: ReactNode;
}

const GlobalTheme: FC<ThemeProviderProps> = ({ children }) => {
  const { theme } = useContext(ThemeContext);

  return (
    <MuiThemeProvider theme={createTheme(getTheme(theme))}>
      <CssBaseline />
      <GlobalStyles styles={globalStyles} />
      {children}
    </MuiThemeProvider>
  );
};

export default GlobalTheme;
