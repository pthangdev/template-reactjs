import { FC, Fragment } from 'react';
import { DefaultLayout, EmptyLayout } from '~/layouts';
import { HomePage, LoginPage, NotFoundPage, RegisterPage } from '~/pages';

export const routePath = {
  home: '/',
  login: '/auth/login',
  register: '/auth/register',
  notfound: '*',
} as const;

interface Route {
  path: string;
  component: FC<any>;
  layout?: FC<any>;
  outer?: FC<any>;
}

const routeConfigs: Route[] = [
  {
    path: routePath.home,
    component: HomePage,
    layout: DefaultLayout,
  },
  {
    path: routePath.login,
    component: LoginPage,
  },
  {
    path: routePath.register,
    component: RegisterPage,
  },
  {
    path: routePath.notfound,
    component: NotFoundPage,
    layout: EmptyLayout,
  },
];

const mapRoutes = (routeConfigs: Route[]) => {
  return routeConfigs.map((routeConfig) => {
    const layout: FC<any> = routeConfig?.layout || DefaultLayout;
    const outer = routeConfig?.outer || Fragment;

    return {
      ...routeConfig,
      layout,
      outer,
    };
  });
};

export const routes = mapRoutes(routeConfigs);
