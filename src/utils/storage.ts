type StorageType = 'local' | 'session';

class Storage {
  storage: globalThis.Storage;
  constructor(storageType: StorageType) {
    this.storage = storageType === 'local' ? localStorage : sessionStorage;
  }

  get<T = string>({ key, parsed = false }: { key: string; parsed?: boolean }): T {
    let value = this.storage.getItem(key);

    if (parsed) {
      value = JSON.parse(value || '""');
    }

    return value as T;
  }

  set({ key, value, stringify = false }: { key: string; value: string; stringify?: boolean }) {
    let isStringify = stringify;
    if (typeof value === 'string' || typeof value === 'boolean' || typeof value === 'number') isStringify = false;
    let newValue = value;

    if (isStringify) {
      newValue = JSON.stringify(value);
    }

    return this.storage.setItem(key, newValue);
  }

  remove(key: string) {
    return this.storage.removeItem(key);
  }

  clear() {
    return this.storage.clear();
  }
}

export const _localStorage = new Storage('local');
export const _sessionStorage = new Storage('session');
