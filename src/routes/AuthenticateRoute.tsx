import { FC, ReactNode } from 'react';

interface AuthenticateRouteProps {
  children: ReactNode;
}

const AuthenticateRoute: FC<AuthenticateRouteProps> = ({ children }) => {
  return <div>{children}</div>;
};

export default AuthenticateRoute;
