import type { FC } from 'react';
import { Route } from 'react-router-dom';
import { routes } from '~/configs/routes';

interface PageRoutesProps {}

const PageRoutes: FC<PageRoutesProps> = (props) => {
  return (
    <div>
      {routes.map((route) => {
        const { path, layout: Layout, outer: Outer, component: Component } = route;

        return (
          <Route
            path={path}
            element={
              <Outer>
                <Layout>
                  <Component />
                </Layout>
              </Outer>
            }
          />
        );
      })}
    </div>
  );
};

export default PageRoutes;
