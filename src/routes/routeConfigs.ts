import { DefaultLayout, EmptyLayout } from '~/layouts';
import LoginPage from '~/pages/auth/login/LoginPage';
import RegisterPage from '~/pages/auth/register/ResgisterPage';
import HomePage from '~/pages/home/HomePage';
import NotFoundPage from '~/pages/notfound/NotFoundPage';
import { AuthenticateRoute, PublicRoute } from '~/routes';

export const routePath = {
  home: '/',
  login: '/auth/login',
  register: '/auth/register'
} as const;

export const routes = [
  {
    label: 'Home',
    path: routePath.home,
    isHideLabel: true,
    component: HomePage,
    outerRoute: PublicRoute
  },
  {
    label: 'Login',
    path: routePath.login,
    isHideLabel: false,
    component: LoginPage,
    layout: DefaultLayout,
    outerRoute: AuthenticateRoute
  },
  {
    label: 'Register',
    path: routePath.register,
    isHideLabel: false,
    component: RegisterPage,
    outerRoute: AuthenticateRoute
  },
  {
    label: 'Not Found',
    path: '*',
    isHideLabel: true,
    layout: EmptyLayout,
    component: NotFoundPage,
    outerRoute: PublicRoute
  }
];
