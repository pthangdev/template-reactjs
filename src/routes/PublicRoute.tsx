import { FC, ReactNode } from 'react';

interface PublicRouteProps {
  children: ReactNode;
}

const PublicRoute: FC<PublicRouteProps> = ({ children }) => {
  return <div>{children}</div>;
};

export default PublicRoute;
