import type { FC, ReactNode } from 'react';

import { createContext, useEffect, useState } from 'react';
import { ThemeMode } from '~/types';
import { _localStorage } from '~/utils';

interface Props {
  children: ReactNode;
}

interface ThemeContextType {
  theme: ThemeMode;
  changeTheme: (theme: ThemeMode) => void;
  toggleTheme: () => void;
}

export const ThemeContext = createContext<ThemeContextType>({
  theme: ThemeMode.light,
  changeTheme: () => {},
  toggleTheme: () => {},
});

export const ThemeProvider: FC<Props> = ({ children }) => {
  const [theme, setTheme] = useState<ThemeMode>(ThemeMode.light);

  const changeTheme = (theme: ThemeMode) => {
    setTheme(theme);

    _localStorage.set({ key: 'theme', value: theme });
  };

  const toggleTheme = () => {
    const themeSelected = theme === ThemeMode.light ? ThemeMode.dark : ThemeMode.light;
    setTheme(themeSelected);
    _localStorage.set({ key: 'theme', value: themeSelected });
  };

  useEffect(() => {
    const themeStorage = _localStorage.get<ThemeMode>({ key: 'theme' });
    if (!ThemeMode[themeStorage]) {
      // Handle event theme in media
      const matchMedia = window.matchMedia('(prefers-color-scheme: dark)');
      matchMedia.addEventListener('change', (event) => {
        const themeSetted = event.matches ? ThemeMode.dark : ThemeMode.light;

        changeTheme(themeSetted);
        setTheme(themeSetted);
      });
    } else {
      setTheme(themeStorage);
    }
  }, []);

  useEffect(() => {
    document.documentElement.setAttribute('data-theme', theme);
  }, [theme]);

  return <ThemeContext.Provider value={{ theme, changeTheme, toggleTheme }}>{children}</ThemeContext.Provider>;
};
